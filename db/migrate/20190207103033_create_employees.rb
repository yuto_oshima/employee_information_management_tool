class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :employee_id
      t.string :name
      t.integer :age
      t.integer :gender
      t.string :address
      t.references :picture, foreign_key: true
      t.references :department, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
