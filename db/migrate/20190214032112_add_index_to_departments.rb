class AddIndexToDepartments < ActiveRecord::Migration[5.1]
  def change
    # 同じcomapany_idの名前をつけなくする
    add_index :departments, [:name, :company_id], unique: true
  end
end
