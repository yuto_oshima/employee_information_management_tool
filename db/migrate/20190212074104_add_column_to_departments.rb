class AddColumnToDepartments < ActiveRecord::Migration[5.1]
  def change
    add_reference :departments, :company, foreign_key: true
  end
end
