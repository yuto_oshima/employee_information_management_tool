class DepartmentsController < ApplicationController
 before_action :authenticate_user!
 before_action :correct_user, only: [:edit, :update, :destroy]
  def index
    company = current_user.company
    @departments = company.departments
  end

  def new
    @department = Department.new
  end

  def create
    company = current_user.company
    @department = company.departments.build(department_params)
    if @department.save
      redirect_to departments_path and return
    else
      # renderは何かを描画するときに使う
      # ここではアクションではなく、"new"のViewを呼んでいる
      render "new"
    end
  end

  def edit
    @department = Department.find(params[:id])
  end

  def update
    @department = Department.find(params[:id])
    if @department.update_attributes(department_params)
      redirect_to departments_url and return
    else
      render "edit"
    end
  end

  def destroy
    Department.find(params[:id]).destroy
    redirect_to departments_url and return
  end

  private
    
    def department_params
      params.require(:department).permit(:name)
    end

    def correct_user
      department = Department.find(params[:id])
      redirect_to departments_path and return unless department.company == current_user.company
    end
      
end
