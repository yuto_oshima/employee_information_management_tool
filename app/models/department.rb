# == Schema Information
#
# Table name: departments
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :bigint(8)
#

class Department < ApplicationRecord
  has_many :employees
  belongs_to :company
  validates :name, presence: true, uniqueness: {scope: [:company_id]}
  validates :company_id, presence: true 
end
