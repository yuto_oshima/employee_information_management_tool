# == Schema Information
#
# Table name: employees
#
#  id            :bigint(8)        not null, primary key
#  employee_id   :string(255)
#  name          :string(255)
#  age           :integer
#  gender        :integer
#  address       :string(255)
#  picture_id    :bigint(8)
#  department_id :bigint(8)
#  company_id    :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Employee < ApplicationRecord
  belongs_to :picture
  belongs_to :department
  belongs_to :company
end
