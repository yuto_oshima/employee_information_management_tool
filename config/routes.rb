Rails.application.routes.draw do

  
  # 自分で指定したcontrollerを見に行く
  # defautlの場合はgemの中にあるcontrollerを見に行く(表側に出ていない)
  # 自由にいじれない
  devise_for :users, :controllers => {
    :registrations => "users/registrations",
    :sessions => "users/sessions"
  }

  resources :departments, except: [:show]

  resources :employees, except: [:index, :show]
  root 'employees#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
